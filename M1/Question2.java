// TimyCars M1 Q2
// Emilien Gâne
// 21 sept 2022

public class Question2 {
    
    public static void main(String[] args) {

    double prixht = 155.55; //Variable pour le prix ht 
    double prixttc; //Variable pour le prix TTC
    double tva = 1.20; //Variable pour la valeur de la TVA

    prixttc = prixht * tva; //Calcule pour le prix TTC

    System.out.println("Le prix de la voiture HT est de " + prixht + ", la TVA est de 20% le prix TTC est de : " + prixttc); //Afficher le prix TTC

        
    }


}
