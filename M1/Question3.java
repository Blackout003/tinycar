// TimyCars M1 Q3
// Emilien Gâne
// 25 sept 2022

import java.util.Scanner;

public class Question3 {
 
    public static void main(String[] args) {
        
        double prixttc; //Variable pour le prix TTC
        double tva = 1.20; //Variable pour la valeur de la TVA

        //Recuperation saisie user
        Scanner sc = new Scanner(System.in);
        System.out.print("Veuillez saisir le prix HT : ");
        double prixht = sc.nextDouble(); //Variable pour le prix ht saisie pas user
        System.out.println("Vous avez saisi : " + prixht);
    
    
        prixttc = prixht * tva; //Calcule pour le prix TTC
    
        System.out.println("Le prix de la voiture HT est de " + prixht + ", la TVA est de 20% le prix TTC est de : " + prixttc); //Afficher le prix TTC



    }


}
