// TimyCars M1 Q1
// Emilien Gâne
// 21 sept 2022


class Question1{
    public static void main (String[] args){

        int age=20; 
        System.out.println("La variable [age] de type [int] contient : " + age); 

        int i=0 ; 
        i=1+1;
        System.out.println("La variable [i] de type [int] contient : " + i); 

        int j=i;
        System.out.println("La variable [j] de type [int] contient : " + j);

        String voiture= "peugeot";
        System.out.println("La variable [voiture] de type [String] contient : " + voiture);

        int k = 0;
        System.out.println("La variable [k] de type [int] contient : " + k); 

        int h=k+k;
        System.out.println("La variable [h] de type [String] contient : " + h); 

        boolean majeur=(age>18);
        System.out.println("La variable [majeur] de type [boolean] contient : " + majeur); 

    }
}