// TimyCars M1 Q4
// Emilien Gâne
// 26 sept 2022

import java.util.Scanner;

public class Question4 {
 
    public static void main(String[] args) {
        
        double prixttc; //Variable pour le prix TTC
        double tva = 1.20; //Variable pour la valeur de la TVA
 
        //Recuperation saisie voiture
        Scanner sc = new Scanner(System.in);
        System.out.print("Veuillez saisir la marque de la voiture : ");
        String marque = sc.nextLine(); //Variable pour la marque
        System.out.println("Vous avez saisi : " + marque);

        //Recuperation saisie model
        System.out.print("Veuillez saisir le model de la voiture : ");
        String model = sc.nextLine(); //Variable pour le model
        System.out.println("Vous avez saisi : " + model);


        //Recuperation saisie prix HT
        System.out.print("Veuillez saisir le prix HT : ");
        double prixht = sc.nextDouble(); //Variable pour le prix ht saisie pas user
        System.out.println("Vous avez saisi : " + prixht);
    
    
        prixttc = prixht * tva; //Calcule pour le prix TTC
    
        System.out.println("Le prix de la voiture HT est de " + prixht + ", la TVA est de 20% le prix TTC est de : " + prixttc); //Afficher le prix TTC

        System.out.println("----Recapitulatif----");    //Recapitulatif des saisie
        System.out.println("Marque de votre voiture : " + marque);      
        System.out.println("Model de votre voiture : " + model);
        System.out.println("Prix HT de votre voiture : " + prixht);
        System.out.println("Valeur de TVA est de : 20% ");
        System.out.println("Prix TTC de votre voiture : " + prixttc);


    }


}
