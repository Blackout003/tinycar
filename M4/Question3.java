// TimyCars M4 Q3
// Emilien Gâne
// 29 nov 2022


import java.util.Scanner;
public class Question3 {
    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        System.out.println("Saisir le nombre de produit : ");//recuperen le nombre de produit
        int panniernmb = sc.nextInt();
        sc.nextLine();

        double [] prix = new double[panniernmb];//creation du tab prix
        String [] produit = new String[panniernmb];//creation du tab produit
        
        double somme=0;//variable pour la somme total

        for(int a=0; a<panniernmb;a++){//recuperation des infos

            
            System.out.println("Saisir le nom du produit "+a);
            produit[a] = sc.nextLine();
            System.out.println("Saisir le prix du "+a);
            prix[a] = sc.nextInt();
            somme += prix[a];
            sc.nextLine();
            
        }
        

        for(int i=0; i<panniernmb; i++){//afficher les produit

            System.out.println("--------Produit_"+i+"----------");
            System.out.println("Le produit est : "+produit[i]);
            System.out.println("Le prix est de : "+prix[i]);
            
        } 

        System.out.println("--------Pannier----------");//afficher le total du pannier
        System.out.print("La somme du pannier est : "+somme);

    }

    
    
}