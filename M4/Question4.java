// TimyCars M4 Q4
// Emilien Gâne
// 29 nov 2022


import java.util.Scanner;
public class Question4 {
    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        System.out.print("Saisir le nombre de produit : ");//recuperent le nombre de produit
        int panniernmb = sc.nextInt();
        sc.nextLine();

        double [] prix = new double[panniernmb];//creation du tab prix
        String [] produit = new String[panniernmb];//creation du tab produit
        
        double somme=0;//variable pour la somme total
        

        for(int a=0; a<panniernmb;a++){//recupet les informations

            
            System.out.print("Saisir le nom du produit "+a+" : ");
            produit[a] = sc.nextLine();
            System.out.print("Saisir le prix du "+a+" : ");
            prix[a] = sc.nextDouble();
            somme += prix[a];
            sc.nextLine();
            
        }
        
        double min=prix[0];//variable pour le prix min
        double max=prix[0];//variable pour le prix max
        String minpro="";//variable pour le produit min
        String maxpro="";//variable pour le produit max

        for(int b=0; b<panniernmb;b++){//calcule le prix min et max

            if(min > prix[b]){

                min=prix[b];
                minpro=produit[b];

            }
            if(max < prix[b]){

                max=prix[b];
                maxpro=produit[b];

            }

        }

        double moy=somme/panniernmb;//caculer le moyenne 
        

        for(int i=0; i<panniernmb; i++){//afficher les produit

            System.out.println("--------Produit_"+i+"----------");
            System.out.println("Le produit est : "+produit[i]);
            System.out.println("Le prix est de : "+prix[i]);
            
        } 

        System.out.println("--------Pannier----------");//afficher le prix total 
        System.out.println("Le total du pannier est de : "+somme);
        System.out.println("--------Statistiques----------");//affiche prix max, min et moyenne 
        System.out.println("Le produit le moins cher est "+minpro+" sont prix est : "+min);
        System.out.println("Le produit le plus cher est "+maxpro+" sont prix est : "+max);
        System.out.println("La moyenne du pannier est de : "+moy);

    }

    
    
}