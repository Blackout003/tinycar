// TimyCars M3 Q1
// Emilien Gâne
// 11 oct 2022

import java.util.Scanner;


public class Question1 {
 
    public static void main(String[] args) {

        boolean restart=true;

     while(restart == true){
        
        double prixttc; //Variable pour le prix TTC
        double tva = 1.20; //Variable pour la valeur de la TVA
        double remise = 0.90;//Variable pour la valeur de la remise
        double prixremis = 20000;//Variable pour le prix TTC avec remise
 
        //Recuperation saisie voiture
        Scanner sc = new Scanner(System.in);
        System.out.print("Veuillez saisir la marque de la voiture : ");
        String marque = sc.nextLine(); //Variable pour la marque
        System.out.println("Vous avez saisi : " + marque);

        //Recuperation saisie model
        System.out.print("Veuillez saisir le model de la voiture : ");
        String model = sc.nextLine(); //Variable pour le model
        System.out.println("Vous avez saisi : " + model);


        //Recuperation saisie prix HT
        System.out.print("Veuillez saisir le prix HT : ");
        double prixht = sc.nextDouble(); //Variable pour le prix ht saisie pas user
        System.out.println("Vous avez saisi : " + prixht);
    
    
        prixttc = prixht * tva; //Calcule pour le prix TTC

        if (prixttc >=20000){//Apliquer la remise si prix sup a 20 000

           prixremis = prixttc * remise;//Calcule pour le prix TTC avec remise
           System.out.println("Le prix de la voiture HT est de " + prixht + ", la TVA est de 20%, le prix TTC avec la remis de 10% est de  : " + prixremis); //Afficher le prix TTC avec remise

        }else{//Apliquer la remise si prix inf a 20 000

            System.out.println("Le prix de la voiture HT est de " + prixht + ", la TVA est de 20% le prix TTC est de : " + prixttc); //Afficher le prix TTC
        }



        //Recapitulatif des saisie
        System.out.println("----Recapitulatif----");    
        System.out.println("Marque de votre voiture : " + marque);      
        System.out.println("Model de votre voiture : " + model);
        System.out.println("Prix HT de votre voiture : " + prixht);
        System.out.println("Valeur de TVA est de : 20% ");
        System.out.println("Vous de benifier pas de la remis ");
        System.out.println("Prix TTC sans remis de votre voiture : " + prixttc);
        if (prixttc >=20000){
         System.out.println("Valeur de remix est de : 10% ");
         System.out.println("Prix TTC avec remis de votre voiture : " + prixremis);
        }

        System.out.print("Vous les vous recomence le programe oui(true) non(false) ");
         restart = sc.nextBoolean();
        }

        if (restart==false){
            System.out.print("Merci d'avoir utiliser le programe");
        }


     }

    


}
