// TimyCars M2 Q3
// Emilien Gâne
// 26 sept 2022

import java.util.Scanner;


public class Question3 {
 
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Veuillez saisir le mot de passe (indice : BTS1) : ");
        String pass = sc.nextLine();//variable pour le type de voiture elecritque(true) ou thermique(false)

     if (pass.equals("BTS1first")){

        
        double prixttc; //Variable pour le prix TTC
        double tva = 1.20; //Variable pour la valeur de la TVA pour le calcule
        double remise = 0.90;//Variable pour la valeur de la remise
        double prixremis = 20000;//Variable pour le prix TTC avec remise

        int TVAa= 20;//Variable pour la valeur de la TVA pour l'affiche user
 
        //Recuperation saisie voiture
        System.out.print("Veuillez saisir la marque de la voiture : ");
        String marque = sc.nextLine(); //Variable pour la marque
        System.out.println("Vous avez saisi : " + marque);

        //Recuperation saisie model
        System.out.print("Veuillez saisir le model de la voiture : ");
        String model = sc.nextLine(); //Variable pour le model
        System.out.println("Vous avez saisi : " + model);


        //Recuperation saisie prix HT
        System.out.print("Veuillez saisir le prix HT : ");
        double prixht = sc.nextDouble(); //Variable pour le prix ht saisie pas user
        System.out.println("Vous avez saisi : " + prixht);

        //Recuperation saisie type de la voiture
        System.out.print("Votre voiture est elecritque(true) ou thermique(false) : ");
        boolean type = sc.nextBoolean();//variable pour le type de voiture elecritque(true) ou thermique(false)
        System.out.println("Vous avez saisi : " + type);

        //Si voiture electrique (true) sinon continu le code normalment
        if (type==true){

            tva=1.05;//mettre tva a 5% pour le calcule
            TVAa = 5;//mettre afichage tva a 5%
        }
    
        prixttc = prixht * tva; //Calcule pour le prix TTC

        if (prixttc >=20000){//Apliquer la remise si prix ttc sup a 20 000

           prixremis = prixttc * remise;//Calcule pour le prix TTC avec remise
           System.out.println("--//--Resultats--//--");
           System.out.println("Le prix de la voiture HT est de " + prixht + ", la TVA est de "+TVAa+"%, le prix TTC avec la remis de 10% est de  : " + prixremis); //Afficher le prix TTC avec remise

        }else{//si prix ttc inf a 20 000 

            System.out.println("--//--Resultats--//--");
            System.out.println("Le prix de la voiture HT est de " + prixht + ", la TVA est de "+TVAa+"%, le prix TTC est de : " + prixttc); //Afficher le prix TTC
        }



        //Recapitulatif des saisie
        System.out.println("--//--Recapitulatif--//--");    
        System.out.println("Marque de votre voiture : " + marque);      
        System.out.println("Model de votre voiture : " + model);

        //afficher voiture thermique ou electrique
        if (type==true){
            System.out.println("Votre voiture est : Electrique");
        }
        else{
            System.out.println("Votre voiture est : Thermique");

        }

        System.out.println("Prix HT de votre voiture : " + prixht);

        //afficher TVA 5% ou 20%
        if (type==true){

             System.out.println("Valeur de TVA est de : 5% ");
        }
        else{

            System.out.println("Valeur de TVA est de : 20% ");
        }

        //affichier que remis non appliquer
        if (prixttc <=20000){
        System.out.println("Vous ne benifier pas de la remis ");
        }
        
        System.out.println("Prix TTC de votre voiture : " + prixttc);

        //affiche remise
        if (prixttc >=20000){
         System.out.println("Valeur de remix est de : 10% ");
         System.out.println("Prix TTC avec remis de votre voiture : " + prixremis);
        }

     }
     else{
        System.out.println("Mot de passe faux");
        System.out.println("inisalisation du programe de securiter");
        System.out.println("Activation dans...");
        System.out.println("4");
        System.out.println("3");
        System.out.println("2");
        System.out.println("1");
        System.out.println("Act5iv84ation e#uror4e pr&o7gr4a5me co6nr&on4p&us !&!4!");
     }

    }


}